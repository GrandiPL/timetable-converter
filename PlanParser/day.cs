﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanParser
{
    class Day
    {
        char[] id = new char[16];
        string name;
        char[] shortcut = new char[2];
        List<int> days = new List<int>();

        public void setId(char[] _id)
        {
            id = _id;
        }
        public void setName(string _name)
        {
            name = _name;
        }
        public void setShortcut(char[] _shortcut)
        {
            shortcut = _shortcut;
        }
        public void setDays(List<int> _days)
        {
            days = _days;
        }

        public char[] getId()
        {
            return id;
        }
        public string getName()
        {
            return name;
        }
        public char[] getShortcut()
        {
            return shortcut;
        }
        public List<int> getDays()
        {
            return days;
        }
    }
}
