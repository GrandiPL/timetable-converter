﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanParser
{
    class Period
    {
        string name;
        string shortcut;
        int period;
        char[] startTime = new char[5];
        char[] endTime = new char[5];

		public string getName(){
			return name;
		}
		public string getShortcut(){
			return shortcut;
		}
		public int getPeriod(){
			return period;
		}
		public char[] getStartTime(){
			return startTime;
		}
		public char[] getEndTime(){
			return endTime;
		}

		public void setName(string _name){
			name = _name;
		}
		

		public void setShortcut(string _shortcut){
			shortcut = _shortcut;
		}
		

		public void setPeriod(int _period){
			period = _period;
		}
		

		public void setStartTime(char[] _startTime){
			startTime = _startTime;
		}
		
		public void setEndTime(char[] _endTime){
			endTime = _endTime;
		}



    }
}
