﻿using System;
using System.Xml;
using System.Web.UI;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace PlanParser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            if (outputPath == "")
            {
                button1.Enabled = false;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK) // File broswer "OK" button press
            {    
                textBox1.Text = openFileDialog1.FileName;
                textBox1.Refresh();
                button1.Enabled = false;
                button2.Enabled = false;
                checkBox1.Enabled = false;
                checkBox2.Enabled = false;
                checkBox3.Enabled = false;
                checkBox4.Enabled = false;
                backgroundWorker1.RunWorkerAsync();
            }
        }
        int progress;
        List<Teacher> teachers = new List<Teacher>(); //Dynamic array of dynamic object
        List<Subject> subjects = new List<Subject>(); //Dynamic array of dynamic object
        List<Day> days = new List<Day>();
        List<Period> periods = new List<Period>();
        List<Classroom> classrooms = new List<Classroom>();
        List<Class> classes = new List<Class>();
        List<Group> groups = new List<Group>();
        List<Lesson> lessons = new List<Lesson>();
        List<Card> cards = new List<Card>();
        String outputPath = "";
        private void readXml()
        {
            XmlTextReader reader = new XmlTextReader(openFileDialog1.FileName); // Creating XML stream reader
            //progressBar1.Maximum = 0;
            textBox1.Text = openFileDialog1.FileName;
            try
            {
                while (reader.Read()) // Stream reader start
                {
                    switch (reader.NodeType) // Checking type of tag (<item> || </item>)
                    {
                        case XmlNodeType.Element:
                            { // (<item>)
                                switch (reader.Name)
                                {
                                    case "teacher":
                                        {
                                            Teacher newTeacher = new Teacher(); // Creating dynamic object

                                            while (reader.MoveToNextAttribute()) // Checking all atributes in tag <item x="">
                                            {
                                                switch (reader.Name)
                                                {
                                                    case "id":
                                                        {
                                                            newTeacher.setId(reader.Value.ToCharArray());
                                                            break;
                                                        }
                                                    case "name":
                                                        {
                                                            newTeacher.setName(reader.Value.ToString());
                                                            break;
                                                        }
                                                    case "short":
                                                        {
                                                            newTeacher.setShortcut(reader.Value.ToCharArray());
                                                            break;
                                                        }
                                                    case "gender":
                                                        {
                                                            newTeacher.setgender(System.Convert.ToChar(reader.Value));
                                                            break;
                                                        }
                                                }
                                            }

                                            teachers.Add(newTeacher); // Adding new dynamic object to array
                                            newTeacher = null; //Deleting dynamic object
                                            break;
                                        }
                                    case "subject":
                                        {
                                            Subject newSubject = new Subject(); // Creating dynamic object
                                            while (reader.MoveToNextAttribute())// Checking all atributes in tag <item x="">
                                            {
                                                switch (reader.Name)
                                                {
                                                    case "id":
                                                        {
                                                            newSubject.setId(reader.Value.ToCharArray());
                                                            break;
                                                        }
                                                    case "name":
                                                        {
                                                            newSubject.setName(reader.Value.ToString());
                                                            break;
                                                        }
                                                    case "short":
                                                        {
                                                            newSubject.setShortcut(reader.Value.ToString());
                                                            break;
                                                        }
                                                }
                                            }
                                            subjects.Add(newSubject); // Adding new dynamic object to array
                                            newSubject = null; //Deleting dynamic object
                                            break;
                                        }
                                    case "daysdef":
                                        {
                                            Day newDay = new Day(); // Creating dynamic object
                                            while (reader.MoveToNextAttribute())// Checking all atributes in tag <item x="">
                                            {
                                                switch (reader.Name)
                                                {
                                                    case "id":
                                                        {
                                                            newDay.setId(reader.Value.ToCharArray());
                                                            break;
                                                        }
                                                    case "name":
                                                        {
                                                            newDay.setName(reader.Value.ToString());
                                                            break;
                                                        }
                                                    case "short":
                                                        {
                                                            newDay.setShortcut(reader.Value.ToCharArray());
                                                            break;
                                                        }
                                                    case "days":
                                                        {
                                                            newDay.setDays(reader.Value.Split(',').ToList().ConvertAll(s => Int32.Parse(s)));
                                                            break;
                                                        }
                                                }
                                            }
                                            days.Add(newDay); // Adding new dynamic object to array
                                            newDay = null; //Deleting dynamic object

                                            break;
                                        }
                                    case "period":
                                        {
                                            Period newPeriod = new Period(); // Creating dynamic object
                                            while (reader.MoveToNextAttribute())// Checking all atributes in tag <item x="">
                                            {
                                                switch (reader.Name)
                                                {
                                                    case "name":
                                                        {
                                                            newPeriod.setName(reader.Value.ToString());
                                                            break;
                                                        }
                                                    case "short":
                                                        {
                                                            newPeriod.setShortcut(reader.Value);
                                                            break;
                                                        }
                                                    case "period":
                                                        {
                                                            newPeriod.setPeriod(System.Convert.ToInt32(reader.Value));
                                                            break;
                                                        }
                                                    case "starttime":
                                                        {
                                                            newPeriod.setStartTime(reader.Value.ToCharArray());
                                                            break;
                                                        }
                                                    case "endtime":
                                                        {
                                                            newPeriod.setEndTime(reader.Value.ToCharArray());
                                                            break;
                                                        }
                                                }
                                            }
                                            periods.Add(newPeriod); // Adding new dynamic object to array
                                            newPeriod = null; //Deleting dynamic object
                                            break;
                                        }
                                    case "classroom":
                                        {
                                            Classroom newClassroom = new Classroom(); // Creating dynamic object
                                            while (reader.MoveToNextAttribute())// Checking all atributes in tag <item x="">
                                            {
                                                switch (reader.Name)
                                                {
                                                    case "id":
                                                        {
                                                            newClassroom.setId(reader.Value.ToCharArray());
                                                            break;
                                                        }
                                                    case "short":
                                                        {
                                                            newClassroom.setShortcut(reader.Value);
                                                            break;
                                                        }
                                                    case "name":
                                                        {
                                                            newClassroom.setName(reader.Value.ToString());
                                                            break;
                                                        }
                                                }
                                            }
                                            classrooms.Add(newClassroom); // Adding new dynamic object to array
                                            newClassroom = null; //Deleting dynamic object
                                            break;
                                        }
                                    case "class":
                                        {
                                            Class newClass = new Class(); // Creating dynamic object
                                            while (reader.MoveToNextAttribute())// Checking all atributes in tag <item x="">
                                            {
                                                switch (reader.Name)
                                                {
                                                    case "id":
                                                        {
                                                            newClass.setId(reader.Value.Take(16).ToArray());
                                                            break;
                                                        }
                                                    case "short":
                                                        {
                                                            newClass.setShortcut(reader.Value);
                                                            break;
                                                        }
                                                    case "name":
                                                        {
                                                            newClass.setName(reader.Value.ToString());
                                                            break;
                                                        }
                                                    case "teacherid":
                                                        {
                                                            newClass.setTeacherId(reader.Value.Take(16).ToArray());
                                                            break;
                                                        }
                                                }
                                            }
                                            classes.Add(newClass); // Adding new dynamic object to array
                                            newClass = null; //Deleting dynamic object
                                            break;
                                        }
                                    case "group":
                                        {
                                            Group newGroup = new Group(); // Creating dynamic object
                                            while (reader.MoveToNextAttribute())// Checking all atributes in tag <item x="">
                                            {
                                                switch (reader.Name)
                                                {
                                                    case "id":
                                                        {
                                                            newGroup.setId(reader.Value.Take(16).ToArray());
                                                            break;
                                                        }
                                                    case "name":
                                                        {
                                                            newGroup.setName(reader.Value.ToString());
                                                            break;
                                                        }
                                                    case "classid":
                                                        {
                                                            newGroup.setClassId(reader.Value.Take(16).ToArray());
                                                            break;
                                                        }
                                                }
                                            }
                                            groups.Add(newGroup); // Adding new dynamic object to array
                                            newGroup = null; //Deleting dynamic object
                                            break;
                                        }
                                    case "lesson":
                                        {
                                            Lesson newLesson = new Lesson(); // Creating dynamic object
                                            while (reader.MoveToNextAttribute())// Checking all atributes in tag <item x="">
                                            {
                                                switch (reader.Name)
                                                {
                                                    case "id":
                                                        {
                                                            newLesson.setId(reader.Value.Take(16).ToArray());
                                                            break;
                                                        }
                                                    case "classids":
                                                        {
                                                            newLesson.setClassId(reader.Value.ToCharArray());
                                                            break;
                                                        }
                                                    case "subjectid":
                                                        {
                                                            newLesson.setSubjectId(reader.Value.Take(16).ToArray());
                                                            break;
                                                        }
                                                    case "teacherids":
                                                        {
                                                            newLesson.setTeacherId(reader.Value.Take(16).ToArray());
                                                            break;
                                                        }
                                                    case "groupids":
                                                        {
                                                            newLesson.setGroupId(reader.Value.ToCharArray());
                                                            break;
                                                        }
                                                    case "classroomids":
                                                        {
                                                            newLesson.setClassroomId(reader.Value.Take(16).ToArray());
                                                            break;
                                                        }
                                                }
                                            }
                                            lessons.Add(newLesson);
                                            break;
                                        }
                                    case "card":
                                        {
                                            Card newCard = new Card(); // Creating dynamic object
                                            while (reader.MoveToNextAttribute())// Checking all atributes in tag <item x="">
                                            {
                                                switch (reader.Name)
                                                {
                                                    case "lessonid":
                                                        {
                                                            newCard.setLessonId(reader.Value.Take(16).ToArray());
                                                            break;
                                                        }
                                                    case "classroomids":
                                                        {
                                                            newCard.setClassroomId(reader.Value.Take(16).ToArray());
                                                            break;
                                                        }
                                                    case "period":
                                                        {
                                                            newCard.setPeriod(System.Convert.ToInt32(reader.Value));
                                                            break;
                                                        }
                                                    case "days":
                                                        {
                                                            newCard.setDays(System.Convert.ToInt32(reader.Value));
                                                            break;
                                                        }
                                                }
                                            }
                                            cards.Add(newCard); // Adding new dynamic object to array
                                            newCard = null; //Deleting dynamic object
                                            break;
                                        }
                                }
                                break;
                            }
                        case XmlNodeType.EndElement:
                            { // (</item>
                                break;
                            }
                    }
                }
                checkDirectoryExists(outputPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not open file. Original error: " + ex.Message);
            }
            reader.Close();
        }
        private void renderClassesHtml(List<Class> _classes, List<Day> _days, List<Period> _periods, List<Group> _groups, List<Lesson> _lessons, List<Card> _cards, List<Classroom> _classrooms, List<Subject> _subjects, List<Teacher> _teachers)
        {
                foreach (Class @class in _classes)
                {
                    StringWriter strWriter = new StringWriter();
                    HtmlTextWriter writer = new HtmlTextWriter(strWriter);
                    StreamWriter htmlFile = new StreamWriter(outputPath + "\\classes\\"+ new string(@class.getId()) + ".html");
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "table table-striped table-hover");
                    writer.RenderBeginTag(HtmlTextWriterTag.Table);//<table>
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);//<tr>
                    writer.RenderBeginTag(HtmlTextWriterTag.Th);//<th>
                    writer.RenderEndTag();//</th>

                    foreach (Day day in _days)
                    {
                        if (day.getDays().Count() == 1 && !day.getDays().Contains(11111))
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                            writer.RenderBeginTag(HtmlTextWriterTag.Th); // <th>
                            writer.Write(day.getShortcut());
                            writer.RenderEndTag();//</th>
                            writer.WriteLine();
                        }

                    }
                    writer.RenderEndTag();// </tr>
                    writer.WriteLine();

                    foreach (Period period in _periods)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Tr); //<tr>
                        writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                        writer.RenderBeginTag(HtmlTextWriterTag.Td);//<td>
                        writer.Write(period.getShortcut() + "<br />" + new string(period.getStartTime().ToArray()) + " - " + new string(period.getEndTime()));
                        writer.RenderEndTag();//</td>
                        foreach (Day day in _days)
                        {
                            if (day.getDays().Count() == 1 && !day.getDays().Contains(11111))
                            {
                                writer.RenderBeginTag(HtmlTextWriterTag.Td);//<td>
                                foreach (Card card in _cards)
                                {
                                    if (card.getDays() == day.getDays()[0])
                                    {
                                        foreach (Lesson lesson in _lessons)
                                        {
                                            if (new string(lesson.getClassId()).Length != 16 && new string(lesson.getGroupId()).Length != 16)
                                            {
                                                int i = 0;
                                                foreach (string _lesson in new string(lesson.getClassId()).Split(','))
                                                {
                                                    if (new string(card.getLessonId()) == new string(lesson.getId()) && card.getPeriod() == period.getPeriod() && new string(@class.getId()) == _lesson)
                                                    {
                                                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "card");
                                                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                                                        foreach (Subject subject in _subjects)
                                                        {
                                                            if (new string(lesson.getSubjectId()) == new string(subject.getId()))
                                                            {
                                                                writer.AddAttribute(HtmlTextWriterAttribute.Class, "subject");
                                                                writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                                writer.Write(subject.getShortcut() + "\t");
                                                                writer.RenderEndTag();//</div>
                                                                writer.WriteLine();
                                                            }
                                                        }
                                                        foreach (Group group in _groups)
                                                        {
                                                            if (new string(lesson.getGroupId()).Split(',')[i] == new string(group.getId()))
                                                            {
                                                                writer.AddAttribute(HtmlTextWriterAttribute.Class, "group");
                                                                writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                                if (group.getName() == "Cała klasa" || group.getName() == "Ca?a klasa")
                                                                {
                                                                    foreach (Class _class in _classes)
                                                                    {
                                                                        if (new string(group.getClassId()) == new string(_class.getId()))
                                                                        {
                                                                            writer.Write(_class.getShortcut());
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    writer.Write(group.getName());
                                                                }
                                                                writer.RenderEndTag();//</div>
                                                                writer.WriteLine();
                                                            }
                                                        }
                                                        foreach (Classroom classroom in _classrooms)
                                                        {
                                                            if (new string(card.getClassroomId()) == new string(classroom.getId()))
                                                            {
                                                                writer.AddAttribute(HtmlTextWriterAttribute.Class, "classroom");
                                                                writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                                if (checkBox2.Checked)
                                                                {
                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#classroom=" + new string(classroom.getId()));
                                                                    writer.RenderBeginTag(HtmlTextWriterTag.A);//<a>
                                                                    writer.Write(new string(classroom.getShortcut().ToCharArray()));
                                                                    writer.RenderEndTag();//</a>
                                                                }
                                                                else
                                                                {
                                                                    writer.Write(classroom.getShortcut().ToString());
                                                                }
                                                                writer.RenderEndTag();//</div>
                                                                writer.WriteLine();
                                                            }
                                                        }
                                                        foreach (Teacher teacher in _teachers)
                                                        {
                                                            if (new string(lesson.getTeacherId()) == new string(teacher.getID()))
                                                            {
                                                                writer.AddAttribute(HtmlTextWriterAttribute.Class, "teacher");
                                                                writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                                if (checkBox3.Checked)
                                                                {
                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#teacher=" + new string(teacher.getID()));
                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Id, "popoverData");
                                                                    writer.AddAttribute("data-content", "<div>Imię i nazwisko:\t" + teacher.getName() + "</div><div>Inicjały:\t" + new string(teacher.getShortcut().ToArray()) + "</div>");
                                                                    if(this.checkBox4.Checked) writer.AddAttribute("rel", "popover");
                                                                    writer.AddAttribute("data-placement", "bottom");
                                                                    writer.AddAttribute("data-original-title", "<center>Dane nauczyciela:</center>");
                                                                    writer.AddAttribute("data-trigger", "hover");
                                                                    writer.RenderBeginTag(HtmlTextWriterTag.A);//<a>
                                                                    writer.Write(teacher.getShortcut());
                                                                    writer.RenderEndTag();//</a>
                                                                }
                                                                else
                                                                {
                                                                    writer.Write(new string(teacher.getShortcut()));
                                                                }

                                                                writer.RenderEndTag();//</div>
                                                            }
                                                        }
                                                        writer.RenderEndTag();
                                                    }
                                                    i++;
                                                }
                                            }
                                            else
                                            {
                                                if (new string(card.getLessonId()) == new string(lesson.getId()) && card.getPeriod() == period.getPeriod() && new string(@class.getId()) == new string(lesson.getClassId()))
                                                    {
                                                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "card");
                                                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                                                        foreach (Subject subject in _subjects)
                                                        {
                                                            if (new string(lesson.getSubjectId()) == new string(subject.getId()))
                                                            {
                                                                writer.AddAttribute(HtmlTextWriterAttribute.Class, "subject");
                                                                writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                                writer.Write(subject.getShortcut() + "\t");
                                                                writer.RenderEndTag();//</div>
                                                                writer.WriteLine();
                                                            }
                                                        }
                                                        if (new string(lesson.getGroupId()).Length != 16)
                                                        {
                                                            int i = 0;
                                                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "group");
                                                            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                            foreach (string sGroup in new string(lesson.getGroupId()).Split(','))
                                                            {
                                                                foreach (Group group in _groups)
                                                                {
                                                                    if (sGroup == new string(group.getId()))
                                                                    {
                                                                        if (group.getName() == "Cała klasa" || group.getName() == "Ca?a klasa")
                                                                        {
                                                                            foreach (Class _class in _classes)
                                                                            {
                                                                                if (new string(group.getClassId()) == new string(_class.getId()))
                                                                                {
                                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#class=" + new string(_class.getId()));
                                                                                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                                                                                    if (i == new string(lesson.getGroupId()).Split(',').Length - 1)
                                                                                        writer.Write(_class.getShortcut());
                                                                                    else
                                                                                        writer.Write(_class.getShortcut() + "<br />");
                                                                                    writer.RenderEndTag();
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            foreach (Class _class in _classes)
                                                                            {
                                                                                if (new string(group.getClassId()) == new string(_class.getId()))
                                                                                {
                                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#classe=" + new string(_class.getId()));
                                                                                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                                                                                    if (i == new string(lesson.getGroupId()).Split(',').Length - 1)
                                                                                    {
                                                                                        if (!group.getName().Contains(_class.getShortcut()))
                                                                                            writer.Write(group.getName() + "_" + _class.getShortcut());
                                                                                        else
                                                                                            writer.Write(group.getName());
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (!group.getName().Contains(_class.getShortcut()))
                                                                                            writer.Write(group.getName() + "_" + _class.getShortcut() + "<br />");
                                                                                        else
                                                                                            writer.Write(group.getName() + "<br />");
                                                                                    }
                                                                                    writer.RenderEndTag();
                                                                                }
                                                                            }
                                                                        }

                                                                    }
                                                                }
                                                                i++;
                                                            }
                                                            writer.RenderEndTag();//</div>
                                                            writer.WriteLine();
                                                        }
                                                        else
                                                        {
                                                            foreach (Group group in _groups)
                                                            {
                                                                if (new string(lesson.getGroupId()) == new string(group.getId()))
                                                                {
                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "group");
                                                                    writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                                    if (group.getName() == "Cała klasa" || group.getName() == "Ca?a klasa")
                                                                    {
                                                                        foreach (Class _class in _classes)
                                                                        {
                                                                            if (new string(group.getClassId()) == new string(_class.getId()))
                                                                            {
                                                                                writer.Write(_class.getShortcut());
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        writer.Write(group.getName());
                                                                    }
                                                                    writer.RenderEndTag();//</div>
                                                                    writer.WriteLine();
                                                                }
                                                            }
                                                        }
                                                        foreach (Classroom classroom in _classrooms)
                                                        {
                                                            if (new string(card.getClassroomId()) == new string(classroom.getId()))
                                                            {
                                                                writer.AddAttribute(HtmlTextWriterAttribute.Class, "classroom");
                                                                writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                                if (checkBox2.Checked)
                                                                {
                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#classroom=" + new string(classroom.getId()));
                                                                    writer.RenderBeginTag(HtmlTextWriterTag.A);//<a>
                                                                    writer.Write(new string(classroom.getShortcut().ToCharArray()));
                                                                    writer.RenderEndTag();//</a>
                                                                }
                                                                else
                                                                {
                                                                    writer.Write(classroom.getShortcut().ToString());
                                                                }
                                                                writer.RenderEndTag();//</div>
                                                                writer.WriteLine();
                                                            }
                                                        }
                                                        foreach (Teacher teacher in _teachers)
                                                        {
                                                            if (new string(lesson.getTeacherId()) == new string(teacher.getID()))
                                                            {
                                                                writer.AddAttribute(HtmlTextWriterAttribute.Class, "teacher");
                                                                writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                                if (checkBox3.Checked)
                                                                {
                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#teacher=" + new string(teacher.getID()));
                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Id, "popoverData");
                                                                    writer.AddAttribute("data-content", "<div>Imię i nazwisko:\t" + teacher.getName() + "</div><div>Inicjały:\t" + new string(teacher.getShortcut().ToArray()) + "</div>");
                                                                    writer.AddAttribute("rel", "popover");
                                                                    writer.AddAttribute("data-placement", "bottom");
                                                                    writer.AddAttribute("data-original-title", "<center>Dane nauczyciela:</center>");
                                                                    writer.AddAttribute("data-trigger", "hover");
                                                                    writer.RenderBeginTag(HtmlTextWriterTag.A);//<a>
                                                                    writer.Write(teacher.getShortcut());
                                                                    writer.RenderEndTag();//</a>
                                                                }
                                                                else
                                                                {
                                                                    writer.Write(new string(teacher.getShortcut()));
                                                                }

                                                                writer.RenderEndTag();//</div>
                                                            }
                                                        }
                                                        writer.RenderEndTag();
                                                    }
                                            }
                                        }
                                    }
                                }
                                writer.RenderEndTag();//</td>
                            }
                        }
                        writer.RenderEndTag();//</tr>
                        writer.WriteLine();
                    }
                    writer.RenderEndTag();//</table>
                    htmlFile.Write(strWriter.ToString());
                    htmlFile.Close();
                    htmlFile = null;
                    strWriter = null;
                    writer = null;
                    progress++;
                    backgroundWorker3.ReportProgress(progress);
                }
        }
        private void renderTeachersHtml(List<Class> _classes, List<Day> _days, List<Period> _periods, List<Group> _groups, List<Lesson> _lessons, List<Card> _cards, List<Classroom> _classrooms, List<Subject> _subjects, List<Teacher> _teachers)
        {
            foreach (Teacher teacher in _teachers)
            {
                StringWriter strWriter = new StringWriter();
                HtmlTextWriter writer = new HtmlTextWriter(strWriter);
                StreamWriter htmlFile = new StreamWriter(outputPath + "\\teachers\\" + new string(teacher.getID()) + ".html");
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "table table-striped table-hover");
                writer.RenderBeginTag(HtmlTextWriterTag.Table);//<table>
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);//<tr>
                writer.RenderBeginTag(HtmlTextWriterTag.Th);//<th>
                writer.RenderEndTag();//</th>
                foreach (Day day in _days)
                {
                    if (day.getDays().Count() == 1 && !day.getDays().Contains(11111))
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                        writer.RenderBeginTag(HtmlTextWriterTag.Th); // <th>
                        writer.Write(day.getShortcut());
                        writer.RenderEndTag();//</th>
                        writer.WriteLine();
                    }

                }
                writer.RenderEndTag();// </tr>
                writer.WriteLine();

                foreach (Period period in _periods)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr); //<tr>
                    writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);//<td>
                    writer.Write(period.getShortcut() + "<br />" + new string(period.getStartTime().ToArray()) + " - " + new string(period.getEndTime()));
                    writer.RenderEndTag();//</td>
                    foreach (Day day in _days)
                    {
                        if (day.getDays().Count() == 1 && !day.getDays().Contains(11111))
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Td);//<td>
                            foreach (Card card in _cards)
                            {
                                if (card.getDays() == day.getDays()[0])
                                {
                                    foreach (Lesson lesson in _lessons)
                                    {
                                        if (new string(card.getLessonId()) == new string(lesson.getId()) && card.getPeriod() == period.getPeriod() && new string(lesson.getTeacherId()) == new string(teacher.getID()))
                                        {
                                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "card");
                                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                                            foreach (Subject subject in _subjects)
                                            {
                                                if (new string(lesson.getSubjectId()) == new string(subject.getId()))
                                                {
                                                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "subject");
                                                    writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                    writer.Write(subject.getShortcut() + "\t");
                                                    writer.RenderEndTag();//</div>
                                                    writer.WriteLine();
                                                }
                                            }
                                            if (new string(lesson.getGroupId()).Length != 16)
                                            {
                                                int i = 0;
                                                writer.AddAttribute(HtmlTextWriterAttribute.Class, "group");
                                                writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                foreach (string sGroup in new string(lesson.getGroupId()).Split(','))
                                                {
                                                    foreach (Group group in _groups)
                                                    {
                                                        if (sGroup == new string(group.getId()))
                                                        {
                                                            if (group.getName() == "Cała klasa" || group.getName() == "Ca?a klasa")
                                                            {
                                                                foreach (Class _class in _classes)
                                                                {
                                                                    if (new string(group.getClassId()) == new string(_class.getId()))
                                                                    {
                                                                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#class=" + new string(_class.getId()));
                                                                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                                                                        if (i == new string(lesson.getGroupId()).Split(',').Length-1)
                                                                            writer.Write(_class.getShortcut());
                                                                        else
                                                                            writer.Write(_class.getShortcut() + "<br />");
                                                                        writer.RenderEndTag();
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                foreach (Class _class in _classes)
                                                                {
                                                                    if (new string(group.getClassId()) == new string(_class.getId()))
                                                                    {
                                                                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#class=" + new string(_class.getId()));
                                                                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                                                                        if (i == new string(lesson.getGroupId()).Split(',').Length-1)
                                                                        {
                                                                            if (!group.getName().Contains(_class.getShortcut()))
                                                                                writer.Write(group.getName() + "_" + _class.getShortcut());
                                                                            else
                                                                                writer.Write(group.getName());
                                                                        }
                                                                        else
                                                                        {
                                                                            if (!group.getName().Contains(_class.getShortcut()))
                                                                                writer.Write(group.getName() + "_" + _class.getShortcut() + "<br />");
                                                                            else
                                                                                writer.Write(group.getName() + "<br />");
                                                                        }
                                                                        writer.RenderEndTag();
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                    }
                                                    i++;
                                                }
                                                writer.RenderEndTag();//</div>
                                                writer.WriteLine();
                                            }
                                            else
                                            {
                                                foreach (Group group in _groups)
                                                {
                                                    if (new string(lesson.getGroupId()) == new string(group.getId()))
                                                    {
                                                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "group");
                                                        writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                        if (group.getName() == "Cała klasa" || group.getName() == "Ca?a klasa")
                                                        {
                                                            foreach (Class _class in _classes)
                                                            {
                                                                if (new string(group.getClassId()) == new string(_class.getId()))
                                                                {
                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#class=" + new string(_class.getId()));
                                                                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                                                                    writer.Write(_class.getShortcut());
                                                                    writer.RenderEndTag();
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            foreach (Class _class in _classes)
                                                            {
                                                                if (new string(group.getClassId()) == new string(_class.getId()))
                                                                {
                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#class=" + new string(_class.getId()));
                                                                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                                                                    if (!group.getName().Contains(_class.getShortcut()))
                                                                        writer.Write(group.getName() + "_" + _class.getShortcut());
                                                                    else
                                                                        writer.Write(group.getName());
                                                                    writer.RenderEndTag();
                                                                }
                                                            }
                                                        }
                                                        writer.RenderEndTag();//</div>
                                                        writer.WriteLine();
                                                    }
                                                }
                                            }
                                            foreach (Classroom classroom in _classrooms)
                                            {
                                                if (new string(card.getClassroomId()) == new string(classroom.getId()))
                                                {
                                                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "classroom");
                                                    writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                    if (checkBox2.Checked)
                                                    {
                                                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#classroom=" + new string(classroom.getId()));
                                                        writer.RenderBeginTag(HtmlTextWriterTag.A);//<a>
                                                        writer.Write(classroom.getShortcut());
                                                        writer.RenderEndTag();//</a>
                                                    }
                                                    else
                                                    {
                                                        writer.Write(classroom.getShortcut());
                                                    }
                                                    writer.RenderEndTag();//</div>
                                                    writer.WriteLine();
                                                }
                                            }
                                            writer.RenderEndTag();
                                        }
                                    }
                                }
                            }
                            writer.RenderEndTag();//</td>
                        }
                    }
                    writer.RenderEndTag();//</tr>
                    writer.WriteLine();
                }
                writer.RenderEndTag();//</table>
                htmlFile.Write(strWriter.ToString());
                htmlFile.Close();
                htmlFile = null;
                strWriter = null;
                writer = null;
                progress++;
                backgroundWorker4.ReportProgress(progress);
            }
        }
        private void renderClassroomsHtml(List<Class> _classes, List<Day> _days, List<Period> _periods, List<Group> _groups, List<Lesson> _lessons, List<Card> _cards, List<Classroom> _classrooms, List<Subject> _subjects, List<Teacher> _teachers)
        {
            foreach (Classroom classroom in _classrooms)
            {
                StringWriter strWriter = new StringWriter();
                HtmlTextWriter writer = new HtmlTextWriter(strWriter);
                StreamWriter htmlFile = new StreamWriter(outputPath + "\\classrooms\\" + new string(classroom.getId()) + ".html");
                htmlFile.AutoFlush = true;
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "table table-striped table-hover");
                writer.RenderBeginTag(HtmlTextWriterTag.Table);//<table>
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);//<tr>
                writer.RenderBeginTag(HtmlTextWriterTag.Th);//<th>
                writer.RenderEndTag();//</th>

                foreach (Day day in _days)
                {
                    if (day.getDays().Count() == 1 && !day.getDays().Contains(11111))
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                        writer.RenderBeginTag(HtmlTextWriterTag.Th); // <th>
                        writer.Write(day.getShortcut());
                        writer.RenderEndTag();//</th>
                        writer.WriteLine();
                    }

                }
                writer.RenderEndTag();// </tr>
                writer.WriteLine();

                foreach (Period period in _periods)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr); //<tr>
                    writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);//<td>
                    writer.Write(period.getShortcut() + "<br />" + new string(period.getStartTime().ToArray()) + " - " + new string(period.getEndTime()));
                    writer.RenderEndTag();//</td>
                    foreach (Day day in _days)
                    {
                        if (day.getDays().Count() == 1 && !day.getDays().Contains(11111))
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Td);//<td>
                            foreach (Card card in _cards)
                            {
                                if (card.getDays() == day.getDays()[0])
                                {
                                    foreach (Lesson lesson in _lessons)
                                    {
                                        if (new string(card.getLessonId()) == new string(lesson.getId()) && card.getPeriod() == period.getPeriod() && new string(card.getClassroomId()) == new string(classroom.getId()))
                                        //if(new string(card.getLessonId()) == new string(lesson.getId()) && )
                                        {
                                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "card");
                                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                                            foreach (Subject subject in _subjects)
                                            {
                                                if (new string(lesson.getSubjectId()) == new string(subject.getId()))
                                                {
                                                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "subject");
                                                    writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                    writer.Write(subject.getShortcut() + "\t");
                                                    writer.RenderEndTag();//</div>
                                                    writer.WriteLine();
                                                }
                                            }
                                            if (new string(lesson.getGroupId()).Length != 16)
                                            {
                                                int i = 0;
                                                writer.AddAttribute(HtmlTextWriterAttribute.Class, "group");
                                                writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                foreach (string sGroup in new string(lesson.getGroupId()).Split(','))
                                                {
                                                    foreach (Group group in _groups)
                                                    {
                                                        if (sGroup == new string(group.getId()))
                                                        {
                                                            if (group.getName() == "Cała klasa" || group.getName() == "Ca?a klasa")
                                                            {
                                                                foreach (Class _class in _classes)
                                                                {
                                                                    if (new string(group.getClassId()) == new string(_class.getId()))
                                                                    {
                                                                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#class=" + new string(_class.getId()));
                                                                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                                                                        if (i == new string(lesson.getGroupId()).Split(',').Length - 1)
                                                                            writer.Write(_class.getShortcut());
                                                                        else
                                                                            writer.Write(_class.getShortcut() + "<br />");
                                                                        writer.RenderEndTag();
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                foreach (Class _class in _classes)
                                                                {
                                                                    if (new string(group.getClassId()) == new string(_class.getId()))
                                                                    {
                                                                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#class=" + new string(_class.getId()));
                                                                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                                                                        if (i == new string(lesson.getGroupId()).Split(',').Length - 1)
                                                                        {
                                                                            if (!group.getName().Contains(_class.getShortcut()))
                                                                                writer.Write(group.getName() + "_" + _class.getShortcut());
                                                                            else
                                                                                writer.Write(group.getName());
                                                                        }
                                                                        else
                                                                        {
                                                                            if (!group.getName().Contains(_class.getShortcut()))
                                                                                writer.Write(group.getName() + "_" + _class.getShortcut() + "<br />");
                                                                            else
                                                                                writer.Write(group.getName() + "<br />");
                                                                        }
                                                                        writer.RenderEndTag();
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }
                                                    i++;
                                                }
                                                writer.RenderEndTag();//</div>
                                                writer.WriteLine();
                                            }
                                            else
                                            {
                                                foreach (Group group in _groups)
                                                {
                                                    if (new string(lesson.getGroupId()) == new string(group.getId()))
                                                    {
                                                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "group");
                                                        writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                        if (group.getName() == "Cała klasa" || group.getName() == "Ca?a klasa")
                                                        {
                                                            foreach (Class _class in _classes)
                                                            {
                                                                if (new string(group.getClassId()) == new string(_class.getId()))
                                                                {
                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#class=" + new string(_class.getId()));
                                                                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                                                                    writer.Write(_class.getShortcut());
                                                                    writer.RenderEndTag();
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            foreach (Class _class in _classes)
                                                            {
                                                                if (new string(group.getClassId()) == new string(_class.getId()))
                                                                {
                                                                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#class=" + new string(_class.getId()));
                                                                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                                                                    if (!group.getName().Contains(_class.getShortcut()))
                                                                        writer.Write(group.getName() + "_" + _class.getShortcut());
                                                                    else
                                                                        writer.Write(group.getName());
                                                                    writer.RenderEndTag();
                                                                }
                                                            }
                                                        }
                                                        writer.RenderEndTag();//</div>
                                                        writer.WriteLine();
                                                    }
                                                }
                                            }
                                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "classroom");
                                            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                            writer.RenderEndTag();//</div>
                                            foreach (Teacher teacher in _teachers)
                                            {
                                                if (new string(lesson.getTeacherId()) == new string(teacher.getID()))
                                                {
                                                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "teacher");
                                                    writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
                                                    if (checkBox3.Checked)
                                                    {
                                                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#teacher=" + new string(teacher.getID()));
                                                        writer.AddAttribute(HtmlTextWriterAttribute.Id, "popoverData");
                                                        writer.AddAttribute("data-content", "<div>Imię i nazwisko:\t" + teacher.getName() + "</div><div>Inicjały:\t" + new string(teacher.getShortcut().ToArray()) + "</div>");
                                                        if (this.checkBox4.Checked) writer.AddAttribute("rel", "popover");
                                                        writer.AddAttribute("data-placement", "bottom");
                                                        writer.AddAttribute("data-original-title", "<center>Dane nauczyciela:</center>");
                                                        writer.AddAttribute("data-trigger", "hover");
                                                        writer.RenderBeginTag(HtmlTextWriterTag.A);//<a>
                                                        writer.Write(teacher.getShortcut());
                                                        writer.RenderEndTag();//</a>
                                                    }
                                                    else
                                                    {
                                                        writer.Write(new string(teacher.getShortcut()));
                                                    }

                                                    writer.RenderEndTag();//</div>
                                                }
                                            }
                                            writer.RenderEndTag();
                                        }
                                    }
                                }
                            }
                            writer.RenderEndTag();//</td>
                        }
                    }
                    writer.RenderEndTag();//</tr>
                    writer.WriteLine();
                }
                writer.RenderEndTag();//</table>
                htmlFile.Write(strWriter.ToString());
                htmlFile.Close();
                htmlFile = null;
                strWriter = null;
                writer = null;
                progress++;
                backgroundWorker5.ReportProgress(progress);
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                outputPath = folderBrowserDialog1.SelectedPath;
                textBox2.Text = outputPath;
            }

        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text != "")
                button1.Enabled = true;
            else
                button1.Enabled = false;
        }
        private void checkDirectoryExists(string path)
        {
            if (!Directory.Exists(path + "\\classes\\"))
            {
                Directory.CreateDirectory(path + "\\classes\\");
            }
            if(!Directory.Exists(path+"\\teachers\\"))
            {
                Directory.CreateDirectory(path + "\\teachers\\");
            }
            if (!Directory.Exists(path + "\\classrooms\\"))
            {
                Directory.CreateDirectory(path + "\\classrooms\\");
            }
            if (!Directory.Exists(path + "\\scripts\\"))
            {
                Directory.CreateDirectory(path + "\\scripts\\");
            }
        }
        private void createCss()
        {
            File.WriteAllText(outputPath + "\\main.css", Resource1.main, Encoding.UTF8);
        }
        private void createJs()
        {
            File.WriteAllText(outputPath + "\\scripts\\simple.js", Resource1.simple,System.Text.UnicodeEncoding.UTF8);
        }
        private void createIndex(List<Class> _classes, List<Classroom> _classrooms, List<Teacher> _teachers)
        {
            StringWriter strWriter = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(strWriter);
            StreamWriter htmlFile = new StreamWriter(outputPath + "\\index.html");
            writer.AddAttribute("lang", "en");
            writer.RenderBeginTag(HtmlTextWriterTag.Html);//<html>
            writer.RenderBeginTag(HtmlTextWriterTag.Head);//<head>
            writer.AddAttribute("charset", "utf-8");
            writer.RenderBeginTag(HtmlTextWriterTag.Meta);//<meta>
            writer.RenderEndTag();//</meta>
            writer.WriteLine();
            writer.AddAttribute("http-equiv", "X-UA-Compatible");
            writer.AddAttribute(HtmlTextWriterAttribute.Content, "IE=edge");
            writer.RenderBeginTag(HtmlTextWriterTag.Meta);//<meta>
            writer.RenderEndTag();//</meta>
            writer.WriteLine();
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "viewport");
            writer.AddAttribute(HtmlTextWriterAttribute.Content, "width=device-width, initial-scale=1");
            writer.RenderBeginTag(HtmlTextWriterTag.Meta);//<meta>
            writer.RenderEndTag();//</meta>
            writer.WriteLine();
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "author");
            writer.AddAttribute(HtmlTextWriterAttribute.Content, "Jakub Wachowiak");
            writer.RenderBeginTag(HtmlTextWriterTag.Meta);//<meta>
            writer.RenderEndTag();//</meta>
            writer.WriteLine();
            writer.RenderBeginTag(HtmlTextWriterTag.Title);//<title>
            writer.Write("ZSŁ - Plan Lekcji");
            writer.RenderEndTag();//</title>
            writer.WriteLine();
            writer.AddAttribute(HtmlTextWriterAttribute.Rel, "stylesheet");
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/css");
            writer.AddAttribute(HtmlTextWriterAttribute.Href, "https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css");
            writer.RenderBeginTag(HtmlTextWriterTag.Link);//<link>
            writer.RenderEndTag();//</link>
            writer.WriteLine();
            writer.AddAttribute(HtmlTextWriterAttribute.Rel, "stylesheet");
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/css");
            writer.AddAttribute(HtmlTextWriterAttribute.Href, "main.css");
            writer.RenderBeginTag(HtmlTextWriterTag.Link);//<link>
            writer.RenderEndTag();//</link>
            writer.WriteLine();
            writer.Write("<!--[if lt IE 9]>\n\t\t\t<script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>\n\t\t\t<script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>\n\t\t<![endif]-->");
            writer.WriteLine();
            writer.AddAttribute(HtmlTextWriterAttribute.Href, "https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/");
            writer.RenderBeginTag(HtmlTextWriterTag.Script);//<script>
            writer.RenderEndTag();//</script>
            writer.WriteLine();
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/css");
            writer.RenderBeginTag(HtmlTextWriterTag.Style);//<style>
            writer.RenderEndTag();//</style>
            writer.WriteLine();
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "holderjs-style");
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/css");
            writer.RenderBeginTag(HtmlTextWriterTag.Style);//<style>
            writer.RenderEndTag();//</style>
            writer.RenderEndTag();//</head>
            writer.WriteLine();
            writer.RenderBeginTag(HtmlTextWriterTag.Body);//<body>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "navbar navbar-inverse navbar-fixed-top");
            writer.AddAttribute("role", "navigation");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "container");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "navbar-header");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "button");
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "navbar-toggle collapsed");
            writer.AddAttribute("data-toggle", "collapse");
            writer.AddAttribute("data-target", ".navbar-collapse");
            writer.RenderBeginTag(HtmlTextWriterTag.Button);//<button>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "sr-only");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);//<span>
            writer.RenderEndTag();//</span>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "icon-bar");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);//<span>
            writer.RenderEndTag();//</span>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "icon-bar");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);//<span>
            writer.RenderEndTag();//</span>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "icon-bar");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);//<span>
            writer.RenderEndTag();//</span>
            writer.RenderEndTag();//</button>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "navbar-brand");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "title");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);//<a>
            writer.Write("");
            writer.RenderEndTag();//</a>
            writer.RenderEndTag();//</div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "collapse navbar-collapse");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "nav navbar-nav");
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);//<ul>
            if (checkBox1.Checked)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown active");
                writer.RenderBeginTag(HtmlTextWriterTag.Li);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown-toggle");
                writer.AddAttribute("data-toggle", "dropdown");
                writer.AddAttribute(HtmlTextWriterAttribute.Href, "#classes");
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write("Klasy");
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "caret");
                writer.RenderBeginTag(HtmlTextWriterTag.Span);//<span>
                writer.RenderEndTag();//</span>
                writer.RenderEndTag();//</a>
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown-menu");
                writer.AddAttribute("role", "menu");
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);//<ul>
                foreach (Class _class in _classes)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);//<li>
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#class=" + new string(_class.getId()));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);//<a>
                    writer.Write(_class.getShortcut());
                    writer.RenderEndTag();//</a>
                    writer.RenderEndTag();//</li>
                    writer.WriteLine();
                }
                writer.RenderEndTag();//</ul>
                writer.RenderEndTag();//</li>
            }
            /////////////////////////////
            if (checkBox3.Checked)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown");
                writer.RenderBeginTag(HtmlTextWriterTag.Li);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown-toggle");
                writer.AddAttribute("data-toggle", "dropdown");
                writer.AddAttribute(HtmlTextWriterAttribute.Href, "#teachers");
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write("Nauczyciele");
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "caret");
                writer.RenderBeginTag(HtmlTextWriterTag.Span);//<span>
                writer.RenderEndTag();//</span>
                writer.RenderEndTag();//</a>
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown-menu");
                writer.AddAttribute("role", "menu");
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);//<ul>
                foreach (Teacher _teacher in _teachers)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);//<li>
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#teacher=" + new string(_teacher.getID()));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);//<a>
                    writer.Write("(" + new string(_teacher.getShortcut()) + ")" + _teacher.getName());
                    writer.RenderEndTag();//</a>
                    writer.RenderEndTag();//</li>
                    writer.WriteLine();
                }
                writer.RenderEndTag();//</ul>
                writer.RenderEndTag();//</li>
            }
            /////////////////////////////
            if (checkBox2.Checked)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown");
                writer.RenderBeginTag(HtmlTextWriterTag.Li);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown-toggle");
                writer.AddAttribute("data-toggle", "dropdown");
                writer.AddAttribute(HtmlTextWriterAttribute.Href, "#clasrooms");
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write("Sale Lekcyjne");
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "caret");
                writer.RenderBeginTag(HtmlTextWriterTag.Span);//<span>
                writer.RenderEndTag();//</span>
                writer.RenderEndTag();//</a>
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown-menu");
                writer.AddAttribute("role", "menu");
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);//<ul>
                foreach (Classroom _classroom in _classrooms)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);//<li>
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#classroom=" + new string(_classroom.getId()));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);//<a>
                    writer.Write(_classroom.getShortcut());
                    writer.RenderEndTag();//</a>
                    writer.RenderEndTag();//</li>
                    writer.WriteLine();
                }
                writer.RenderEndTag();//</ul>
                writer.RenderEndTag();//</li>
            }
            writer.RenderEndTag();//</ul>
            writer.RenderEndTag();//</div>
            writer.RenderEndTag();//</div>
            writer.RenderEndTag();//</div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "container-fluid");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "error");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel panel-danger");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel-heading");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel-title");
            writer.RenderBeginTag(HtmlTextWriterTag.H3);//<h3>
            writer.RenderEndTag();//</h3>
            writer.RenderEndTag();//</div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel-body");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
            writer.RenderEndTag();//</div>
            writer.RenderEndTag();//</div>
            writer.RenderEndTag();//</div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "table-responsive");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "data");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>
            writer.RenderEndTag();//</div>
            writer.RenderEndTag();//</div>
            writer.AddAttribute(HtmlTextWriterAttribute.Src, "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js");
            writer.RenderBeginTag(HtmlTextWriterTag.Script);//<script>
            writer.RenderEndTag();//</script>
            writer.AddAttribute(HtmlTextWriterAttribute.Src, "https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js");
            writer.RenderBeginTag(HtmlTextWriterTag.Script);//<script>
            writer.RenderEndTag();//</script>
            writer.AddAttribute(HtmlTextWriterAttribute.Src, "http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js");
            writer.RenderBeginTag(HtmlTextWriterTag.Script);//<script>
            writer.RenderEndTag();//</script>
            writer.AddAttribute(HtmlTextWriterAttribute.Src,"scripts/simple.js");
            writer.RenderBeginTag(HtmlTextWriterTag.Script);
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            htmlFile.Write(strWriter.ToString());
            htmlFile.Close();
            htmlFile = null;
            strWriter = null;
            writer = null;
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            readXml();
            createCss();
            createJs();
            createIndex(classes, classrooms, teachers);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.Maximum = classes.Count + teachers.Count + classrooms.Count;
            progress = 0;
            backgroundWorker2.RunWorkerAsync();
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            if(checkBox1.Checked)
                backgroundWorker3.RunWorkerAsync();
            if(checkBox3.Checked)
                backgroundWorker4.RunWorkerAsync();
            if(checkBox2.Checked)
                backgroundWorker5.RunWorkerAsync();
        }

        private void backgroundWorker3_DoWork(object sender, DoWorkEventArgs e)
        {
            renderClassesHtml(classes, days, periods, groups, lessons, cards, classrooms, subjects, teachers);
            
        }

        private void backgroundWorker4_DoWork(object sender, DoWorkEventArgs e)
        {
            renderTeachersHtml(classes, days, periods, groups, lessons, cards, classrooms, subjects, teachers);
        }

        private void backgroundWorker5_DoWork(object sender, DoWorkEventArgs e)
        {
            renderClassroomsHtml(classes, days, periods, groups, lessons, cards, classrooms, subjects, teachers);
        }

        private void backgroundWorker5_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate
            {
            progressBar1.Value = e.ProgressPercentage;
            label3.Text = (progress * 100 / progressBar1.Maximum).ToString() + "%";
            });
        }

        private void backgroundWorker4_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate
            {
                progressBar1.Value = e.ProgressPercentage;
                label3.Text = (progress * 100 / progressBar1.Maximum).ToString() + "%";
            });
        }

        private void backgroundWorker3_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate
            {
                progressBar1.Value = e.ProgressPercentage;
                label3.Text = (progress * 100 / progressBar1.Maximum).ToString() + "%";
            });
        }

        private void backgroundWorker5_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!backgroundWorker3.IsBusy && !backgroundWorker4.IsBusy && !backgroundWorker5.IsBusy)
            {
                classes = null;
                days = null;
                periods = null;
                groups = null;
                lessons = null;
                cards = null;
                classrooms = null;
                subjects = null;
                teachers = null;
                
                BeginInvoke((MethodInvoker)delegate
                {
                    button2.Enabled = true;
                    checkBox1.Enabled = true;
                    checkBox2.Enabled = true;
                    checkBox3.Enabled = true;
                    checkBox4.Enabled = true;
                    progressBar1.Value = 0;
                    progressBar1.Maximum = 1;
                });
            }
        }

        private void backgroundWorker4_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!backgroundWorker3.IsBusy && !backgroundWorker4.IsBusy && !backgroundWorker5.IsBusy)
            {
                classes = null;
                days = null;
                periods = null;
                groups = null;
                lessons = null;
                cards = null;
                classrooms = null;
                subjects = null;
                teachers = null;
                
                BeginInvoke((MethodInvoker)delegate
                {
                    button2.Enabled = true;
                    checkBox1.Enabled = true;
                    checkBox2.Enabled = true;
                    checkBox3.Enabled = true;
                    checkBox4.Enabled = true;
                    progressBar1.Value = 0;
                    progressBar1.Maximum = 1;
                });
            }
        }

        private void backgroundWorker3_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!backgroundWorker3.IsBusy && !backgroundWorker4.IsBusy && !backgroundWorker5.IsBusy)
            {
                classes = null;
                days = null;
                periods = null;
                groups = null;
                lessons = null;
                cards = null;
                classrooms = null;
                subjects = null;
                teachers = null;
                BeginInvoke((MethodInvoker)delegate
                {
                    button2.Enabled = true;
                    checkBox1.Enabled = true;
                    checkBox2.Enabled = true;
                    checkBox3.Enabled = true;
                    checkBox4.Enabled = true;
                    progressBar1.Value = 0;
                    progressBar1.Maximum = 1;
                });
            }
        }
    }
}