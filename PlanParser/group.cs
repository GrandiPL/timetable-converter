﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanParser
{
    class Group
    {
    	char[] id = new char[16];
    	string name;
    	char[] classId = new char[16];

		public char[] getId(){
			return id;
		}
		public string getName(){
			return name;
		}
		public char[] getClassId(){
			return classId;
		}

		public void setId(char[] _id){
			id = _id;
		}
		public void setName(string _name){
			name = _name;
		}
		public void setClassId(char[] _classId){
			classId = _classId;
		}

    }
}
