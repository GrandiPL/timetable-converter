﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanParser
{
    class Card
    {
        char[] lessonId = new char[16];
        char[] classroomId = new char[16];
        int period;
        int days;

		public char[] getLessonId(){
			return lessonId;
		}
		public char[] getClassroomId(){
			return classroomId;
		}
		public int getPeriod(){
			return period;
		}
		public int getDays(){
			return days;
		}

		public void setLessonId(char[] _lessonId){
			lessonId = _lessonId;
		}
		public void setClassroomId(char[] _classroomId){
			classroomId = _classroomId;
		}
		public void setPeriod(int _period){
			period = _period;
		}
		public void setDays(int _days){
			days = _days;
		}
    }
}
