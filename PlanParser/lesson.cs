﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanParser
{
    class Lesson
    {
        char[] id = new char[16];
        char[] classId = new char[16];
        char[] subjectId = new char[16];
        char[] teacherId = new char[16];
        char[] groupId = new char[16];
        char[] classroomId = new char[16];

		public char[] getId(){
			return id;
		}
		public char[] getClassId(){
			return classId;
		}
		public char[] getSubjectId(){
			return subjectId;
		}
		public char[] getTeacherId(){
			return teacherId;
		}
		public char[] getGroupId(){
			return groupId;
		}
        public char[] getClassroomId()
        {
            return classroomId;
        }

		public void setId(char[] _id){
			id = _id;
		}
		public void setClassId(char[] _classId){
			classId = _classId;
		}
		public void setSubjectId(char[] _subjectId){
			subjectId = _subjectId;
		}
		public void setTeacherId(char[] _teacherId){
			teacherId = _teacherId;
		}
		public void setGroupId(char[] _groupId){
			groupId = _groupId;
		}
        public void setClassroomId(char[] _classroomId)
        {
            classroomId = _classroomId;
        }

    }
}
