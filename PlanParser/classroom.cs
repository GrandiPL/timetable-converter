﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanParser
{
    class Classroom
    {
        char[] id = new char[16];
        string name;
        string shortcut;

		public char[] getId(){
			return id;
		}

		public string getName(){
			return name;
		}

		public string getShortcut(){
			return shortcut;
		}

		public void setId(char[] _id){
			id = _id;
		}
		public void setName(string _name){
			name = _name;
		}
		public void setShortcut(string _shortcut){
			shortcut = _shortcut;
		}

    }
}
