﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlanParser
{
   class Teacher
    {
        char[] id = new char[16];
        string name;
        char[] shortcut = new char[2];
        char gender;

        public void setId(char[] _id)
        {
            id = _id;
        }
        public void setName(string _name)
        {
            name = _name;
        }
        public void setShortcut(char[] _shortcut)
        {
            shortcut = _shortcut;
        }
        public void setgender(char _gender)
        {
            gender = _gender;
        }
        public char[] getID()
        {
            return id;
        }
        public string getName()
        {
            return name;
        }
        public char[] getShortcut()
        {
            return shortcut;
        }
        public char getGender()
        {
            return gender;
        }
    }
}
