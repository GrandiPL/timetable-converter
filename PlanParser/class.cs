﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanParser
{
    class Class
    {
        char[] id = new char[16];
        string name;
        string shortcut;
        char[] teacherId = new char[16];

		public char[] getId(){
			return id;
		}
		public string setName(){
			return name;
		}
		public string getShortcut(){
			return shortcut;
		}
		public char[] getTeacherId(){
			return teacherId;
		}        

		public void setId(char[] _id){
			id = _id;
		}
		public void setName(string _name){
			name = _name;
		}
		public void setShortcut(string _shortcut){
			shortcut = _shortcut;
		}
		public void setTeacherId(char[] _teacherId){
			teacherId = _teacherId;
		}

    }
}
