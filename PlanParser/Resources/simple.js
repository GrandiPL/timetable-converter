function displayError(errorNumber, errorClass){
	var title = "<strong>" + errorNumber + " Wystąpił błąd</strong>";
	switch(errorNumber){
		case 404:{
			var msg = "<strong>Strona o podanym adresie nie została znaleziona!</strong>";
			break;
		}
	}
	$("#title").text("Błąd!");
	$("#data").empty();
	$(errorClass+" div.panel.panel-danger div.panel-body").html(msg);
	$(errorClass+" div.panel.panel-danger div.panel-heading h3.panel-title").html(title);
	$(errorClass).show();
}
function convertDays(currentDayNumber){
		var currentDay = "";
	switch(currentDayNumber){
		case 1:
			currentDay = "Poniedziałek";
			break;
		case 2:
			currentDay = "Wtorek";
			break;
		case 3:
			currentDay = "Środa";
			break;
		case 4:
			currentDay = "Czwartek";
			break;
		case 5:
			currentDay = "Piątek";
			break;
	}
	return currentDay.toString();
}
function highlightCurrentDay(){
	$('td, th').each(function(){
		if($(this).index()==currentDate()){
			$(this).addClass("today");
		}
	});
}
function currentDate(){
	var date = new Date();
	return date.getDay();
}
function loadTable(){
	var hashUrl = window.location.hash;
	if(hashUrl.length <= 0){
		$("#title").text($('li.dropdown.active ul.dropdown-menu li:first a').text());
		$('li.dropdown.active ul.dropdown-menu li:first').addClass("active");
		window.location.hash = $('li.dropdown.active ul.dropdown-menu li:first a').attr('href');
	}else{
		var urlParams = window.location.hash.substring(1).split("&");
		var isData = false;
			$.each(urlParams, function(index, value){
				var temp = value.split("=");
				var current = $('li.dropdown ul.dropdown-menu li a[href$="#' + temp[0] + '=' + temp[1] +'"]');
					switch(temp[0]){
						case "class":{
							$("#title").text(current.text());
							$("#data").load(temp[0]+"es/"+temp[1]+".html table", function(response, status, xhr){
								if(status == "error"){
									displayError(xhr.status,"#error");
								}else{
									$('#error').hide();
									$("li.dropdown ul.dropdown-menu li.active").removeClass("active");
									$(current).parent().addClass("active");
									$("li.dropdown.active").removeClass("active");
									$(current).parents("li.dropdown").addClass("active");
								}
							});
							return false;
							break;
						}
						case "teacher":{
							$("#title").text(current.text());
							$("#data").load(temp[0]+"s/"+temp[1]+".html table", function(response, status, xhr){
								if(status == "error"){
									displayError(xhr.status,"#error");
								}else{
									$('#error').hide();
									var address = window.location.hash.substring(1).split("&")[0].split('=');
									$("li.dropdown ul.dropdown-menu li.active").removeClass("active");
									$(current).parent().addClass("active");
									$("li.dropdown.active").removeClass("active");
									$(current).parents("li.dropdown").addClass("active");
								}
							});
							return false;
							break;
						}
						case "teacher":{
							$("#title").text(current.text());
							$("#data").load(temp[0]+"s/"+temp[1]+".html table", function(response, status, xhr){
								if(status == "error"){
									displayError(xhr.status,"#error");
								}else{
									$('#error').hide();
									var address = window.location.hash.substring(1).split("&")[0].split('=');
									$("li.dropdown ul.dropdown-menu li.active").removeClass("active");
									$(current).parent().addClass("active");
									$("li.dropdown.active").removeClass("active");
									$(current).parents("li.dropdown").addClass("active");
								}
							});
							return false;
							break;
						}
						case "classroom":{
							$("#title").text(current.text());
							$("#data").load(temp[0]+"s/"+temp[1]+".html table", function(response, status, xhr){
								if(status == "error"){
									displayError(xhr.status,"#error");
								}else{
									$('#error').hide();
									var address = window.location.hash.substring(1).split("&")[0].split('=');
									$("li.dropdown ul.dropdown-menu li.active").removeClass("active");
									$(current).parent().addClass("active");
									$("li.dropdown.active").removeClass("active");
									$(current).parents("li.dropdown").addClass("active");
								}
							});
							return false;
							break;
						}
						default:{
							return true;
						}
					
					}
					temp =[];
			});
		urlParams = [];
	}
}

$(document).ready(function(){
	loadTable();
	$('li.dropdown ul.dropdown-menu li a').click(function(){
		$('#error').hide();
		$("#title").text(this.text);
		$("li.dropdown ul.dropdown-menu li.active").removeClass("active");
		$(this).parent('li').addClass("active");
		$("li.dropdown.active").removeClass("active");
		$(this).parent('li').parent("ul.dropdown-menu").parent("li.dropdown").addClass("active");
		$("button.navbar-toggle").addClass("collapsed");
		$("div.navbar-collapse.collapse.in").removeClass("in");
	});
});

$(document).ajaxComplete(function(){
	$("[rel=popover]").popover({html : true});
	highlightCurrentDay();
});

$(window).on('hashchange', function(){
	loadTable();
	$("button.navbar-toggle").addClass("collapsed");
	$("navbar-collapse collapse in").removeClass("in");
});
